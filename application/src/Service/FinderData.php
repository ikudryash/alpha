<?php

namespace App\Service;

use Elastica\Query;

class FinderData
{
    public function findApartment(array $data): Query
    {
        $query = new Query();
        $boolQuery = new \Elastica\Query\BoolQuery();

        $numberOfRoomsQuery = new \Elastica\Query\Term();
        $numberOfRoomsQuery->setTerm('design.numberOfRooms', $data['numberOfRooms']);
        $boolQuery->addMust($numberOfRoomsQuery);

        $priceQuery = new \Elastica\Query\Range();
        $priceQuery->addField('price', [
            'lte' => $data['price'],
            'gte' => 0
        ]);
        $boolQuery->addMust($priceQuery);

        $squareQuery = new \Elastica\Query\Range();
        $squareQuery->addField('design.square',[ 'gte' => $data["square"]]);
        $boolQuery->addMust($squareQuery);

        $balconyQuery = new \Elastica\Query\Term();
        $balconyQuery->setTerm('design.balcony', $data['balcony']);
        $boolQuery->addMust($balconyQuery);

        $query->setQuery($boolQuery);

        return $query;
    }
}