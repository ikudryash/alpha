<?php

namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class FilterFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numberOfRooms', ChoiceType::class, [
                'choices' => [
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                    '4' => 4
                ],
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('square', IntegerType::class, [
                'attr' => [
                    'min' => 20,
                    'max' => 150
                ],
                'constraints' => [
                    new Range([
                        'min' => 20,
                        'max' => 150
                    ])
                ]
            ])
            ->add('price', IntegerType::class,[
                'attr' => [
                    'min' => 10000,
                    'max' => 100000
                ],
                'constraints' => [
                    new Range([
                        'min' => 10000,
                        'max' => 100000
                    ])
                ]
            ])
            ->add('balcony', ChoiceType::class, [
                'choices' => [
                    'Да' => true,
                    'Нет' => false
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
//        $resolver->setDefaults([
//            'data_class' => Apartment::class,
//        ]);
    }
}
