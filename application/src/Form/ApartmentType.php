<?php

namespace App\Form;

use App\Entity\Apartment;
use App\Entity\Design;
use App\Entity\HouseSection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class ApartmentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number', IntegerType::class, [
                'constraints' => [
                    new NotNull()
                ]
            ])
            ->add('floor', IntegerType::class,[
                'constraints' => [
                    new NotNull()
                ]
            ])
            ->add('houseSection', EntityType::class, [
                'class' => HouseSection::class,
                'choice_label' => 'id',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('design', EntityType::class,[
                'class' => Design::class,
                'choice_label' => 'id',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('status', ChoiceType::class, [
                'choices' => [
                    Apartment::STATUS_LABELS[Apartment::STATUS_FREE] => Apartment::STATUS_FREE,
                    Apartment::STATUS_LABELS[Apartment::STATUS_RESERVE] => Apartment::STATUS_RESERVE,
                    Apartment::STATUS_LABELS[Apartment::STATUS_SOLD] => Apartment::STATUS_SOLD
                ]
            ])
            ->add('price', IntegerType::class, [
                'constraints' => [
                    new NotNull()
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Apartment::class,
        ]);
    }
}
