<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * @ORM\Entity(repositoryClass="App\Repository\ApartmentRepository")
 * @Vich\Uploadable
 */
class Apartment
{
    const STATUS_FREE = 1;
    const STATUS_RESERVE = 2;
    const STATUS_SOLD = 3;

    const STATUS_LABELS = [
        self::STATUS_FREE => 'Свободная',
        self::STATUS_RESERVE => 'Забронирована',
        self::STATUS_SOLD => 'Продана',
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $number;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $floor;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
     private $status;
    /**
     * @ORM\ManyToOne(targetEntity="HouseSection", inversedBy="apartments")
     * @ORM\JoinColumn(name="house_section_id", referencedColumnName="id")
     */
    private $houseSection;

    /**
     * @ORM\ManyToOne(targetEntity="Design", inversedBy="apartments")
     * @ORM\JoinColumn(name="design_id", referencedColumnName="id")
     */
    private $design;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $price;

    /**
     * @return int
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDesign()
    {
        return $this->design;
    }

    /**
     * @param mixed $design
     */
    public function setDesign(Design $design): void
    {
        $this->design = $design;
        $this->design->addApartment($this);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getNumber(): ?int
    {
        return $this->number;
    }

    /**
     * @param int $number
     */
    public function setNumber(int $number): void
    {
        $this->number = $number;
    }

    /**
     * @return int
     */
    public function getFloor(): ?int
    {
        return $this->floor;
    }

    /**
     * @param int $floor
     */
    public function setFloor(int $floor): void
    {
        $this->floor = $floor;
    }

    /**
     * @return mixed
     */
    public function getHouseSection()
    {
        return $this->houseSection;
    }

    /**
     * @param HouseSection $houseSection
     */
    public function setHouseSection(HouseSection $houseSection): void
    {
        $this->houseSection = $houseSection;
        $this->houseSection->addApartment($this);
    }

    /**
     * @return int
     */
    public function getPrice(): ?int
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price): void
    {
        $this->price = $price;
    }
}
