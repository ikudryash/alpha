<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DesignRepository")
 * @Vich\Uploadable
 */
class Design
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float", precision=2)
     *
     * @var float
     */
    private $square;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $numberOfRooms;

    /**
     * @Vich\UploadableField(mapping="apartments", fileNameProperty="planImage")
     */
    private $planImageFile;

    /**
     * @ORM\Embedded(class="Vich\UploaderBundle\Entity\File")
     *
     * @var File
     */
    private $planImage;

    /**
     * @ORM\OneToMany(targetEntity="Apartment", mappedBy="design")
     */
    private $apartments;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var boolean
     */
    private $balcony;

    /**
     * HouseSection constructor.
     */
    public function __construct()
    {
        $this->apartments = new ArrayCollection();
    }

    public function getApartments(): ArrayCollection
    {
        return $this->apartments;
    }

    /**
     * @param Apartment $apartment
     */
    public function addApartment(Apartment $apartment): void
    {
        $this->apartments->add($apartment);
    }

    /**
     * @param Apartment $apartment
     */
    public function remoteApartment(Apartment $apartment): void
    {
        $this->apartments->removeElement($apartment);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getSquare(): ?float
    {
        return $this->square;
    }

    /**
     * @param float $square
     */
    public function setSquare(float $square): void
    {
        $this->square = $square;
    }

    /**
     * @return int
     */
    public function getNumberOfRooms(): ?int
    {
        return $this->numberOfRooms;
    }

    /**
     * @param int $numberOfRooms
     */
    public function setNumberOfRooms(int $numberOfRooms): void
    {
        $this->numberOfRooms = $numberOfRooms;
    }

    /**
     * @return null|File
     */
    public function getPlanImageFile(): ?File
    {
        return $this->planImageFile;
    }

    /**
     * @param null|File $planImageFile
     */
    public function setPlanImageFile(?File $planImageFile = null): void
    {
        $this->planImageFile = $planImageFile;
    }

    /**
     * @param null|string $planImage
     */
    public function setPlanImage(?string $planImage): void
    {
        $this->planImageFile = $planImage;
    }

    /**
     * @return null|string
     */
    public function getPlanImage(): ?string
    {
        return $this->planImage;
    }

    /**
     * @return bool
     */
    public function getBalcony(): bool
    {
        return $this->balcony;
    }

    /**
     * @return bool
     */
    public function setBalcony(bool $balcony): void
    {
        $this->balcony = $balcony;
    }
}
