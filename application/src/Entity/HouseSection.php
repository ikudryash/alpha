<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HouseSectionRepository")
 */
class HouseSection
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $floors;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $apartmentsNumber;

    /**
     * @ORM\OneToMany(targetEntity="Apartment", mappedBy="houseSection")
     */
    private $apartments;

    /**
     * HouseSection constructor.
     */
    public function __construct()
    {
        $this->apartments = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getFloors(): ?int
    {
        return $this->floors;
    }

    /**
     * @param int $floors
     */
    public function setFloors(int $floors): void
    {
        $this->floors = $floors;
    }

    /**
     * @return int
     */
    public function getApartmentsNumber(): ?int
    {
        return $this->apartmentsNumber;
    }

    /**
     * @param int $apartmentsNumber
     */
    public function setApartmentsNumber(int $apartmentsNumber): void
    {
        $this->apartmentsNumber = $apartmentsNumber;
    }

    /**
     * @return ArrayCollection
     */
    public function getApartments(): ArrayCollection
    {
        return $this->apartments;
    }

    /**
     * @param Apartment $apartment
     */
    public function addApartment(?Apartment $apartment): void
    {
        $this->apartments->add($apartment);
    }

    /**
     * @param Apartment $apartment
     */
    public function remoteApartment(?Apartment $apartment): void
    {
        $this->apartments->removeElement($apartment);
    }
}
