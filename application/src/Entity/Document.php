<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DocumentRepository")
 * @Vich\Uploadable()
 * @ORM\HasLifecycleCallbacks()
 */
class Document
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @Vich\UploadableField(mapping="documents", fileNameProperty="document.name")
     *
     */
    private $documentFile;

    /**
     * @ORM\Embedded(class="Vich\UploaderBundle\Entity\File")
     *
     * @var EmbeddedFile
     */
    private $document;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    protected $createdAt;

    /**
     * Document constructor.
     */
    public function __construct()
    {
        $this->document = new EmbeddedFile();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @param string $filePath
     */
    public function setFilePath($filePath): void
    {
        $this->filePath = $filePath;
    }

    /**
     * @param \DateTime $time
     */
    public function setCreatedAt(\DateTime $time): void
    {
        $this->createdAt = $time;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PrePersist()
     * @throws \Exception
     */
    public function updateTimestamps(): void
    {
        $time = new \DateTime('now');

        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt($time);
        }
    }

    /**
     * @return null|EmbeddedFile
     */
    public function getDocument(): ?EmbeddedFile
    {
        return $this->document;
    }

    /**
     * @param EmbeddedFile $document
     */
    public function setDocument(EmbeddedFile $document): void
    {
        $this->document = $document;
    }

    /**
     * @return null|File
     */
    public function getDocumentFile(): ?File
    {
        return $this->documentFile;
    }

    /**
     * @param File|null $documentFile
     */
    public function setDocumentFile(?File $documentFile = null): void
    {
        $this->documentFile = $documentFile;
    }

    /**
     * @ORM\PrePersist()
     * @throws \Exception
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }
}
