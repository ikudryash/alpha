<?php

namespace App\Controller;

use App\Entity\Document;
use App\Repository\DocumentRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserController
 * @package App\Controller
 */
class LandingController extends Controller
{
    /**
     * @return Response
     *
     * @Route(path="/", name="landing_index")
     */
    public function indexAction(): Response
    {
        return $this->render('landing/index.html.twig');
    }

    /**
     * @param Request $req
     * @param DocumentRepository $documentRepository
     * @return Response
     *
     * @Route(path="/documentation", name="landing_documentation")
     */
    public function documentationAction(Request $req, DocumentRepository $documentRepository): Response
    {
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $documentRepository->findAll(),
            $req->query->getInt('page', 1),
            10
        );

        return $this->render('landing/documentation.html.twig', [
            'pagination' => $pagination,
            'documents_directory' => $this->getParameter('documents_directory')
        ]);
    }

    /**
     * @return Response
     *
     * @Route(path="/construction-progress", name="landing_construction_progress")
     */
    public function constructionProgressAction(): Response
    {
        return $this->render('landing/constructionProgress.html.twig');
    }

    /**
     * @param Request $req
     * @return Response
     *
     * @Route(path="/du")
     */
    public function duAction(Request $req): Response
    {
        $document = new Document();
        /** @var UploadedFile $file */
        $file = $req->files->get('file');
        $document->setTitle($file->getClientOriginalName());
        $filePath = $this->get('app.file_uploader')->upload($file);
        $document->setFilePath($filePath);
        $em = $this->getDoctrine()->getManager();
        $em->persist($document);
        $em->flush();

        return new Response(json_encode(['status' => 200]));
    }
}
