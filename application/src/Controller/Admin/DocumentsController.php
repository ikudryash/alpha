<?php

namespace App\Controller\Admin;

use App\Entity\Apartment;
use App\Entity\Document;
use App\Form\ApartmentType;
use App\Form\DocumentType;
use App\Repository\DocumentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/documents")
 */
class DocumentsController extends Controller
{
    /**
     * @param Request $request
     * @param DocumentRepository $documentRepository
     * @return Response
     *
     * @Route("/", name="documents_index", methods="GET")
     */
    public function indexAction(Request $request, DocumentRepository $documentRepository): Response
    {
        $apartment = $documentRepository->findAll();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $apartment,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('documents/index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @param Request $req
     * @return Response
     *
     * @Route("/new", name="documents_new", methods="GET|POST")
     */
    public function newAction(Request $req): Response
    {
        $document = new Document();
        $form = $this->createForm(DocumentType::class, $document);
        $form->handleRequest($req);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($document);
            $em->flush();
        }

        return $this->render('documents/new.html.twig', [
            'document' => $document,
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Document $document
     * @return Response
     *
     * @Route("/{id}", name="documents_show", methods="GET")
     */
    public function showAction(Document $document): Response
    {
        return $this->render('documents/show.html.twig', [
            'document' => $document
        ]);
    }

    /**
     * @param Request $req
     * @param Document $document
     * @return Response
     *
     * @Route("/{id}/edit", name="documents_edit", methods="GET|POST")
     */
    public function editAction(Request $req, Document $document): Response
    {
        $form = $this->createForm(DocumentType::class, $document);
        $form->handleRequest($req);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('documents_edit', ['id' => $document->getId()]);
        }

        return $this->render('documents/edit.html.twig', [
            'document' => $document,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $req
     * @param Document $document
     * @return Response
     *
     * @Route("/{id}", name="documents_delete", methods="DELETE")
     */
    public function deleteAction(Request $req, Document $document): Response
    {
        if ($this->isCsrfTokenValid('delete'.$document->getId(), $req->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($document);
            $em->flush();
        }

        return $this->redirectToRoute('documents_index');
    }
}
