<?php

namespace App\Controller\Admin;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/article")
 */
class ArticleController extends Controller
{
    /**
     * @param Request $request
     * @param ArticleRepository $articleRepository
     * @return Response
     *
     * @Route("/", name="article_index", methods="GET")
     */
    public function indexAction(Request $request, ArticleRepository $articleRepository): Response
    {
        $acticles = $articleRepository->findAll();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $acticles,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('article/index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @Route("/new", name="article_new", methods="GET|POST")
     */
    public function newAction(Request $request): Response
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $article->setDate(new \DateTime('now'));
            $url = $article->getTitle();
            $url = mb_strtolower($url);
            $url = str_replace(" ","-",$url);
            $article->setUrl($url);
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('article_index');
        }

        return $this->render('article/new.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Article $article
     * @return Response
     *
     * @Route("/{id}", name="article_show", methods="GET")
     */
    public function showAction(Article $article): Response
    {
        return $this->render('article/show.html.twig', ['article' => $article]);
    }

    /**
     * @param Request $request
     * @param Article $article
     * @return Response
     *
     * @Route("/{id}/edit", name="article_edit", methods="GET|POST")
     */
    public function editAction(Request $request, Article $article): Response
    {
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($article);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('article_edit', [
                'id' => $article->getId()
            ]);
        }

        return $this->render('article/edit.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param Article $article
     * @return Response
     *
     * @Route("/{id}", name="article_delete", methods="DELETE")
     */
    public function deleteAction(Request $request, Article $article): Response
    {
        if ($this->isCsrfTokenValid('delete'.$article->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($article);
            $em->flush();
        }

        return $this->redirectToRoute('article_index');
    }
}
