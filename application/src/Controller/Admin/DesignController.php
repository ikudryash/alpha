<?php

namespace App\Controller\Admin;

use App\Entity\Design;
use App\Form\DesignType;
use App\Repository\DesignRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/design")
 */
class DesignController extends AbstractController
{
    /**
     * @param DesignRepository $designRepository
     * @return Response
     *
     * @Route("/", name="design_index", methods="GET")
     */
    public function indexAction(DesignRepository $designRepository): Response
    {
        return $this->render('design/index.html.twig', ['designs' => $designRepository->findAll()]);
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @Route("/new", name="design_new", methods="GET|POST")
     */
    public function newAction(Request $request): Response
    {
        $design = new Design();
        $form = $this->createForm(DesignType::class, $design);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($design);
            $em->flush();

            return $this->redirectToRoute('design_index');
        }

        return $this->render('design/new.html.twig', [
            'design' => $design,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Design $design
     * @return Response
     *
     * @Route("/{id}", name="design_show", methods="GET")
     */
    public function showAction(Design $design): Response
    {
        return $this->render('design/show.html.twig', ['design' => $design]);
    }

    /**
     * @param Request $request
     * @param Design $design
     * @return Response
     *
     * @Route("/{id}/edit", name="design_edit", methods="GET|POST")
     */
    public function editAction(Request $request, Design $design): Response
    {
        $form = $this->createForm(DesignType::class, $design);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('design_edit', ['id' => $design->getId()]);
        }

        return $this->render('design/edit.html.twig', [
            'design' => $design,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param Design $design
     * @return Response
     *
     * @Route("/{id}", name="design_delete", methods="DELETE")
     */
    public function deleteAction(Request $request, Design $design): Response
    {
        if ($this->isCsrfTokenValid('delete'.$design->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($design);
            $em->flush();
        }

        return $this->redirectToRoute('design_index');
    }
}
