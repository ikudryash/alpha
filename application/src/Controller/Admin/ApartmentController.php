<?php

namespace App\Controller\Admin;

use App\Entity\Apartment;
use App\Form\ApartmentType;
use App\Form\FilterFormType;
use App\Repository\ApartmentRepository;
use App\Service\FinderData;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/apartment")
 */
class ApartmentController extends Controller
{
    /**
     * @param Request $request
     * @param FinderData $finderData
     * @return Response
     *
     * @Route("/filter", name="apartment_filter", methods="GET|POST")
     */
    public function findAction(Request $request, FinderData $finderData) : Response
    {
        $defaultData = [];
        $form = $this->createForm(FilterFormType::class, $defaultData);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $query = $finderData->findApartment($data);
            $datas = $this->container->get('fos_elastica.finder.app.apartment')->find($query);

            return $this->render('apartment/filter.html.twig', [
                'form' => $form->createView()
            ]);
        }

        return $this->render('apartment/filter.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param ApartmentRepository $apartmentRepository
     * @return Response
     *
     * @Route("/", name="apartment_index", methods="GET")
     */
    public function indexAction(Request $request, ApartmentRepository $apartmentRepository): Response
    {
        $apartment = $apartmentRepository->findAll();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $apartment,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('apartment/index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @Route("/new", name="apartment_new", methods="GET|POST")
     */
    public function newAction(Request $request): Response
    {
        $apartment = new Apartment();
        $form = $this->createForm(ApartmentType::class, $apartment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($apartment);
            $em->flush();

            return $this->redirectToRoute('apartment_index');
        }

        return $this->render('apartment/new.html.twig', [
            'apartment' => $apartment,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Apartment $apartment
     * @return Response
     *
     * @Route("/{id}", name="apartment_show", methods="GET")
     */
    public function showAction(Apartment $apartment): Response
    {
        return $this->render('apartment/show.html.twig', [
            'apartment' => $apartment,
            'status_label' => Apartment::STATUS_LABELS[$apartment->getStatus()]
        ]);
    }

    /**
     * @param Request $request
     * @param Apartment $apartment
     * @return Response
     *
     * @Route("/{id}/edit", name="apartment_edit", methods="GET|POST")
     */
    public function editAction(Request $request, Apartment $apartment): Response
    {
        $form = $this->createForm(ApartmentType::class, $apartment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('apartment_edit', ['id' => $apartment->getId()]);
        }

        return $this->render('apartment/edit.html.twig', [
            'apartment' => $apartment,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param Apartment $apartment
     * @return Response
     *
     * @Route("/{id}", name="apartment_delete", methods="DELETE")
     */
    public function deleteAction(Request $request, Apartment $apartment): Response
    {
        if ($this->isCsrfTokenValid('delete'.$apartment->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($apartment);
            $em->flush();
        }

        return $this->redirectToRoute('apartment_index');
    }
}
