<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181026134406 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE apartment ADD plan_image_name VARCHAR(255) DEFAULT NULL, ADD plan_image_original_name VARCHAR(255) DEFAULT NULL, ADD plan_image_mime_type VARCHAR(255) DEFAULT NULL, ADD plan_image_size INT DEFAULT NULL, ADD plan_image_dimensions LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE apartment DROP plan_image_name, DROP plan_image_original_name, DROP plan_image_mime_type, DROP plan_image_size, DROP plan_image_dimensions');
    }
}
