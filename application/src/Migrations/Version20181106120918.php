<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181106120918 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE design (id INT AUTO_INCREMENT NOT NULL, square DOUBLE PRECISION NOT NULL, number_of_rooms INT NOT NULL, plan_image_name VARCHAR(255) DEFAULT NULL, plan_image_original_name VARCHAR(255) DEFAULT NULL, plan_image_mime_type VARCHAR(255) DEFAULT NULL, plan_image_size INT DEFAULT NULL, plan_image_dimensions LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE apartment DROP square, DROP number_of_rooms, DROP plan_image_name, DROP plan_image_original_name, DROP plan_image_mime_type, DROP plan_image_dimensions, CHANGE plan_image_size design_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE apartment ADD CONSTRAINT FK_4D7E6854E41DC9B2 FOREIGN KEY (design_id) REFERENCES design (id)');
        $this->addSql('CREATE INDEX IDX_4D7E6854E41DC9B2 ON apartment (design_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE apartment DROP FOREIGN KEY FK_4D7E6854E41DC9B2');
        $this->addSql('DROP TABLE design');
        $this->addSql('DROP INDEX IDX_4D7E6854E41DC9B2 ON apartment');
        $this->addSql('ALTER TABLE apartment ADD square DOUBLE PRECISION NOT NULL, ADD number_of_rooms INT NOT NULL, ADD plan_image_name VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD plan_image_original_name VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD plan_image_mime_type VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD plan_image_dimensions LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:simple_array)\', CHANGE design_id plan_image_size INT DEFAULT NULL');
    }
}
