<?php

namespace App\Repository;

use App\Entity\HouseSection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method HouseSection|null find($id, $lockMode = null, $lockVersion = null)
 * @method HouseSection|null findOneBy(array $criteria, array $orderBy = null)
 * @method HouseSection[]    findAll()
 * @method HouseSection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HouseSectionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, HouseSection::class);
    }

//    /**
//     * @return HouseSection[] Returns an array of HouseSection objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HouseSection
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
