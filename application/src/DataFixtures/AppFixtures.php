<?php

namespace App\DataFixtures;

use App\Entity\Design;
use App\Entity\HouseSection;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Article;


/**
 * Class AppFixtures
 * @package App\DataFixtures
 */
class AppFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $json = file_get_contents('./src/DataFixtures/datas/dataNov-3-2018.json');
        $data = json_decode($json);

        foreach ($data as $datafield) {
            foreach($datafield as $field) {
                $article = new Article();
                $article->setTitle($field[0]);
                $article->setDate(new \DateTime('now'));
                $article->setTextarticle($field[2]);
                $url = mb_strtolower($field[0]);
                $url = str_replace(" ", "-", $url);
                $article->setUrl($url);
                $manager->persist($article);
            }
        }

        //Чтоб потестить
        for ($i = 0; $i < 4; $i++){
            $design = new Design();
            $design->setSquare(($i + 1) * 1.8);
            $design->setNumberOfRooms($i + 1);
            if ($i % 2 == 0) {
                $design->setBalcony( true );
            } else {
                $design->setBalcony( false );
            }

            $manager->persist($design);
        }
        //Чтоб поетстить
        for ($i = 0; $i < 4; $i++){
            $houseSection = new HouseSection();
            $houseSection->setFloors(10);
            $houseSection->setApartmentsNumber(($i + 1) * 10);
            $manager->persist($houseSection);
        }


        $manager->flush();

    }
}
