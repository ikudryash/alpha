var sliderActive = false;
var footerActive = false;
var deltaScroll = {
    newIntensity: 0,
    lastIntensity: 0,
    deltaIntensity: 0,
    newTime: 0,
    lastTime: 0,
    deltaTime: 0
};

function Slider(id) {
    this.activeSlide = 0;
    this.slidersArray = $("#" + id + " .slider-item");
    this.slidersCount = this.slidersArray.length;
    this.sliderBlock = $("#" + id);
    this.delay = false;

    this.prevSlide = function() {
        if (this.activeSlide === 0) {

        } else if (!this.delay) {

            this.slidersArray.eq(this.activeSlide).removeClass('active-slide');
            this.slidersArray.eq(this.activeSlide - 1).addClass('active-slide');

            this.delay = true;
            this.sliderBlock.addClass("change-slide");
            setTimeout(() => {
                this.sliderBlock.removeClass("change-slide");
            }, 1000);

            setTimeout(() => {
                this.sliderBlock.removeClass("change-slide");
                this.delay = false;
            }, 1000);

            this.activeSlide--;
        }

        sliderScrollFix();
    };

    this.nextSlide = function() {
        if (!(this.activeSlide === (this.slidersCount - 1)) && !this.delay) {
            this.slidersArray.eq(this.activeSlide).removeClass('active-slide');
            this.slidersArray.eq(this.activeSlide + 1).addClass('active-slide');

            this.delay = true;
            this.sliderBlock.addClass("change-slide");
            setTimeout(() => {
                this.sliderBlock.removeClass("change-slide");
            }, 1000);

            setTimeout(() => {
                this.sliderBlock.removeClass("change-slide");
                this.delay = false;
            }, 1000);

            this.activeSlide++;
        }

        sliderScrollFix();
    };
};

function sliderScrollFix() {
    if (slider.activeSlide === 0) {
        pageSections.eq(slider.blockId + 1).removeClass('fp-section');

        setTimeout(() => {
            pageSections.eq(slider.blockId - 1).addClass('fp-section');
    }, 1000);

    } else if (slider.activeSlide === slider.slidersCount - 1) {
        pageSections.eq(slider.blockId - 1).removeClass('fp-section');


        setTimeout(() => {
            pageSections.eq(slider.blockId + 1).addClass('fp-section');
    }, 1000);
    } else {
        pageSections.eq(slider.blockId - 1).removeClass('fp-section');
        pageSections.eq(slider.blockId + 1).removeClass('fp-section');
    }
}

var pageSections, slider;

document.onwheel = function (ev) {
    deltaScroll.lastTime = deltaScroll.newTime;
    deltaScroll.newTime = new Date().getTime();
    deltaScroll.lastIntensity = deltaScroll.newIntensity;
    deltaScroll.newIntensity = ev.deltaY;
    deltaScroll.deltaTime = deltaScroll.newTime - deltaScroll.lastTime;
    deltaScroll.deltaIntensity = deltaScroll.newIntensity - deltaScroll.lastIntensity;

    if (!sliderActive) {
        return;
    }

    if (deltaScroll.deltaTime < 20 && deltaScroll.deltaIntensity < 5) {
        return;
    }

    if (ev.deltaY > 0) {
        slider.nextSlide();
    } else {
        slider.prevSlide();
    }
};

$( document ).ready(function() {
    document.body.addEventListener('touchmove',function(e){
        e.preventDefault();});

    slider = new Slider("slider");
    pageSections = $("#fullpage .section");

    for (var i = 0; i < pageSections.length; i++) {
        if (pageSections.eq(i).hasClass('slider')) {
            slider.blockId = i;
        }
    }

    $('body').swipe({
        swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
            checkFooterSrcroll(direction);

            if (!sliderActive) {
                return;
            }

            if (direction === 'down') {
                slider.prevSlide();
            } else if (direction === 'up') {
                slider.nextSlide();
            }
        }
    });
});
