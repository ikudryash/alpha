
function filtersInit() {
    var squareSlider, costSlider, updateSliderValue;

    updateSliderValue = function(slider, type) {
        switch (type) {
            case 'cost':

                var values = slider.noUiSlider.get();
                var minBlock = slider.getElementsByClassName('range-filter__min-point');
                var maxBlock = slider.getElementsByClassName('range-filter__max-point');

                console.log(maxBlock);

                minBlock[0].innerHTML = Math.round(values[0]).toLocaleString('ru');
                maxBlock[0].innerHTML = Math.round(values[1]).toLocaleString('ru');

                break;
            default:
                var children, i, results, val, values;
                children = slider.getElementsByClassName('noUi-handle');
                values = slider.noUiSlider.get();
                i = 0;
                results = [];
                while (i < children.length) {
                    if (children.length === 1) {
                        val = parseInt(values);
                    } else {
                        val = parseInt(values[i]);
                    }
                    children[i].dataset.value = val;
                    results.push(i++);
                }
                return results;
                break;
        }


    };



    squareSlider = document.getElementById('filter-square');

    noUiSlider.create(squareSlider, {
        animationDuration: 300,
        start: [0, 150],
        step: 1,
        range: {
            'min': 0,
            'max': 150
        },
        margin: 33
    });

    squareSlider.noUiSlider.on('update', function(values, handle) {
        return updateSliderValue(squareSlider, 'square');
    });


    costSlider = document.getElementById('filter-cost');

    noUiSlider.create(costSlider, {
        animationDuration: 300,
        start: [1800000, 12125000],
        step: 10000,
        range: {
            'min': 1800000,
            'max': 12125000
        },
        margin: 2300000
    });

    costSlider.noUiSlider.on('update', function() {
        return updateSliderValue(costSlider, 'cost');
    });

}



$(document).ready(function () {
    filtersInit();
});