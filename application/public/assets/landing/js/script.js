const maxFloor = 14;

var selectedFloor = 4;

function selectFloor(element) {
    setFloor($(element)[0]);
}

function headerColorChange(scrolled) {
    var windowHeight = window.innerHeight;
    $(".header").addClass('dark');
    //$(".header").addClass('shadow');

    if ($('div').is('.apartments-page') || $('div').is('.documentation')) {
        if (scrolled > 0) {
            $(".header").addClass('shadow');
        } else {
            $(".header").removeClass('shadow');
        }
    }

    if ($('div').is('.about-page')) {
        if (scrolled < windowHeight) {
            $('.header').removeClass('dark');
            $(".header").removeClass('shadow');
        } else {
            $(".header").addClass('shadow');
        }
    }

    if (scrolled + 2 >= $('.footer').offset().top) {
        $('.header').removeClass('dark');
        $('.header').removeClass('shadow');
    }
}

function openNews(id) {
    $('.news-page__news-card-container').addClass('active');
    $('.header').addClass('shadow');
    //$('.news-card').addClass('active');
    $("body").css("overflow","hidden");
}

function closeNews() {
    $('.news-page__news-card-container').removeClass('active');
    $('.header').removeClass('shadow');
    //$('.news-card').removeClass('active');
    $("body").css("overflow","auto");
}

function selectFlat(element) {
    console.log($(element).data('id'));
}

function selectSection(element) {
    console.log($(element).data('id'));
}

function setFloor(id) {
    if (1 <= id <= maxFloor) {
        selectedFloor = id;
        $('polygon.selected').removeClass('selected');
        $('.floor-counter__count').html(selectedFloor);
        var floor = $('.select-floor-block__select-floors').eq(1).children('g').eq(selectedFloor - 1).children('polygon');
        floor.addClass('selected');
    }
}

function floorsCounterInit() {
    var floor = $('.select-floor-block__select-floors').eq(1).children('g').eq(selectedFloor - 1).children('polygon');

    $('.floor-counter__prev-btn').click(function () {
        if (selectedFloor > 1) {
            selectedFloor--;
            $('.floor-counter__count').html(selectedFloor);
            setFloor(selectedFloor);
        }
    });

    $('.floor-counter__next-btn').click(function () {
        if (selectedFloor < maxFloor) {
            selectedFloor++;
            selectFloor(selectedFloor);
        }
    });
}

function mobileMenuInit() {
    $('.mobile-menu__burger label').click(function () {
        $('.mobile-menu').toggleClass('active');

        if ($('.mobile-menu').hasClass('active')) {
            $.fn.fullpage.setMouseWheelScrolling(false);
            $.fn.fullpage.setAllowScrolling(false);
        } else {
            $.fn.fullpage.setMouseWheelScrolling(true);
            $.fn.fullpage.setAllowScrolling(true);
        }
    });
}

function setCirclesScale() {
    var width = $(window).width();
    var height = $(window).height();
    console.log(width);
    console.log(height);
    var squared = false;

    if (width / height < 1.5) {
        $('#slider').addClass('square');
        squared = true;
    } else {
        $('#slider').removeClass('square');
        squared = false;
    }

    if (width < 1370 && width > 680) {
        $('#slider').addClass('notebook');
    } else {
        $('#slider').removeClass('notebook');
    }

    if (width > 2600) {
        if (squared) {
            $('.slider__circles-container').css('transform', ' scale(' + 2.2 + ')');
            $('.slider-item__container-wrapper').css('transform', ' scale(' + 2.2 + ')');
        } else {
            $('.slider__circles-container').css('transform', ' scale(' + 2.3 + ')');
            $('.slider-item__container-wrapper').css('transform', ' scale(' + 2.3 + ')');
        }
    } else if (width > 2300) {
        if (squared) {
            $('.slider__circles-container').css('transform', ' scale(' + 2.2 + ')');
            $('.slider-item__container-wrapper').css('transform', ' scale(' + 2.2+ ')');
        } else {
            $('.slider__circles-container').css('transform', ' scale(' + 2.2 + ')');
            $('.slider-item__container-wrapper').css('transform', ' scale(' + 2.2+ ')');
        }
    } else if (width > 2000) {
        if (squared) {
            $('.slider__circles-container').css('transform', ' scale(' + 2.2 + ')');
            $('.slider-item__container-wrapper').css('transform', ' scale(' + 2.2+ ')');
        } else {
            $('.slider__circles-container').css('transform', ' scale(' + 2 + ')');
            $('.slider-item__container-wrapper').css('transform', ' scale(' + 2 + ')');
        }
    }  else if (width > 1600) {
        if (squared) {
            $('.slider__circles-container').css('transform', ' scale(' + 1.8 + ')');
            $('.slider-item__container-wrapper').css('transform', ' scale(' + 1.8 + ')');
        } else {
            $('.slider__circles-container').css('transform', ' scale(' + 1.7 + ')');
            $('.slider-item__container-wrapper').css('transform', ' scale(' + 1.7 + ')');
        }
    } else if (width > 1400) {
        if (squared) {
            $('.slider__circles-container').css('transform', ' scale(' + 1.7 + ')');
            $('.slider-item__container-wrapper').css('transform', ' scale(' + 1.7 + ')');
        } else {
            $('.slider__circles-container').css('transform', ' scale(' + 1.5 + ')');
            $('.slider-item__container-wrapper').css('transform', ' scale(' + 1.5 + ')');
        }
    } else if (width > 1200) {
        if (squared) {
            $('.slider__circles-container').css('transform', ' scale(' + 1.6 + ')');
            $('.slider-item__container-wrapper').css('transform', ' scale(' + 1.6 + ')');
        } else {
            $('.slider__circles-container').css('transform', ' scale(' + 1.3 + ')');
            $('.slider-item__container-wrapper').css('transform', ' scale(' + 1.3 + ')');
        }
    } else if (width > 1000) {
        if (squared) {
            $('.slider__circles-container').css('transform', ' scale(' + 1.2 + ')');
            $('.slider-item__container-wrapper').css('transform', ' scale(' + 1.2 + ')');
        } else {
                $('.slider__circles-container').css('transform', ' scale(' + 1.2 + ')');
                $('.slider-item__container-wrapper').css('transform', ' scale(' + 1.2 + ')');
        }
    } else if (width > 800) {
        if (squared) {
            $('.slider__circles-container').css('transform', ' scale(' + 1 + ')');
            $('.slider-item__container-wrapper').css('transform', ' scale(' + 1 + ')');
        } else {
            $('.slider__circles-container').css('transform', ' scale(' + 1 + ')');
            $('.slider-item__container-wrapper').css('transform', ' scale(' + 1 + ')');
        }
    } else if (width > 700) {
        if (squared) {
            $('.slider__circles-container').css('transform', ' scale(' + 1 + ')');
            $('.slider-item__container-wrapper').css('transform', ' scale(' + 1 + ')');
        } else {
            $('.slider__circles-container').css('transform', ' scale(' + 0.8 + ')');
            $('.slider-item__container-wrapper').css('transform', ' scale(' + 0.8 + ')');
        }
    }else if (width > 600) {
        if (squared) {
            $('.slider__circles-container').css('transform', ' scale(' + 1 + ')');
            $('.slider-item__container-wrapper').css('transform', ' scale(' + 1 + ')');
        } else {
            $('.slider__circles-container').css('transform', ' scale(' + 0.7 + ')');
            $('.slider-item__container-wrapper').css('transform', ' scale(' + 0.7 + ')');
        }
    } else if (width > 500) {
        if (squared) {
            $('.slider__circles-container').css('transform', ' scale(' + 0.8 + ')');
            $('.slider-item__container-wrapper').css('transform', ' scale(' + 0.8 + ')');
        } else {
            $('.slider__circles-container').css('transform', ' scale(' + 0.6 + ')');
            $('.slider-item__container-wrapper').css('transform', ' scale(' + 0.6 + ')');
        }
    } else if (width < 500) {
        if (squared) {
            $('.slider__circles-container').css('transform', ' scale(' + 0.8 + ')');
            $('.slider-item__container-wrapper').css('transform', ' scale(' + 0.8 + ')');
        } else {
            $('.slider__circles-container').css('transform', ' scale(' + 0.7 + ')');
            $('.slider-item__container-wrapper').css('transform', ' scale(' + 0.7 + ')');
        }
    }
}

function checkFooterSrcroll(swipeDirection) {
    var container = $('.main-page__footer-container .fp-tableCell')
    var scrollTop = container.scrollTop();

    if (footerActive) {
        if (scrollTop > 0) {
            $.fn.fullpage.setMouseWheelScrolling(false);
            $.fn.fullpage.setAllowScrolling(false);
        } else {
            $.fn.fullpage.setMouseWheelScrolling(true);
            $.fn.fullpage.setAllowScrolling(true);
        }

        if (swipeDirection !== undefined) {
            if (swipeDirection === 'up') {
                $.fn.fullpage.setMouseWheelScrolling(false);
                $.fn.fullpage.setAllowScrolling(false);
                container.scrollTop(1);
            }
        }
    } else {
        $.fn.fullpage.setMouseWheelScrolling(true);
        $.fn.fullpage.setAllowScrolling(true);
    }
}

$(window).resize(function () {
    setCirclesScale();
});

$(document).scroll(function (value1, value2) {
    var windowHeight = window.innerHeight;

    var scrolled = $(document).scrollTop();
    headerColorChange(scrolled);
});

$(document).ready(function () {
    mobileMenuInit();

    if ($('div').is('#fullpage')) {
        $('#fullpage').fullpage({
            css3: true,
            scrollingSpeed: 1000,

            onLeave: function (origin, destination, direction) {
                var id = origin;
                id += destination === 2 ? 1 : -1;

                if ($('.fp-section').eq(id - 1).hasClass('slider')) {
                    $('.header').addClass('dark');
                } else {
                    $('.header').removeClass('dark');
                }
            },

            afterLoad: function (origin, destination, direction) {
                var loadedSection = this;

                if ($(this).hasClass('slider')) {
                    sliderScrollFix();
                    sliderActive = true;
                } else {
                    sliderActive = false;
                }

                if ($(this).hasClass('main-page__footer-container')) {
                    footerActive = true;
                    $('.main-page__footer-container .fp-tableCell').scrollTop(1);
                } else {
                    $('.main-page__footer-container .fp-tableCell').scrollTop(0);
                    footerActive = false;
                }
            }
        });

        $('.main-page__footer-container .fp-tableCell').scroll(function () {
            checkFooterSrcroll();
        })
    }

    setCirclesScale();

    if ($('div').is('.content-block__img-container--slider.owl-carousel')) {
        $('.owl-carousel').owlCarousel({
            loop: true,
            nav: true,
            items: 1,
            rtl: true,
            rewindNav: true,
            navText: ['', '']
        });

        $('.owl-stage-outer').wrap('<div class="content-slider__slide-wrapper"></div>')
    }


    if ($('div').is('.news-card__close-btn')) {
        $('#news-card__close-btn').click(function () {
            closeNews();

        })
    }

    $('.news-item__title').click(function () {
        openNews($(this).data('id'));


    });

    if ($('div').is('.floor-counter')) {
        floorsCounterInit()
    }
});