function cloudAction() {
    var cluds = $('.material-clouds__cloud');
    var windowHeight = window.innerHeight;

    window.onscroll = function() {
        var scrolled = window.pageYOffset || document.documentElement.scrollTop;
        cluds.eq(1).css('left', -(scrolled/windowHeight)* 40 + 0.6 + "%");
        cluds.eq(2).css('left', -(scrolled/windowHeight)* 55 + 30.2 + "%");
        cluds.eq(3).css('right', -(scrolled/windowHeight)* 60 + 29.2  + "%");
        cluds.eq(4).css('right', -(scrolled/windowHeight)* 40 + 2.6 + "%");
    };
}

$(document).ready(function () {
    cloudAction();
});
